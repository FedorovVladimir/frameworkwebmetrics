import java.util.LinkedHashMap;
import java.util.Map;

public class Site {

    private String url;
    private Map<String, Integer> links = new LinkedHashMap<String, Integer>();

    public Site(String url) {
        this.url = url;
    }

    private int insert;

    public void add(String end) {
        if (links.get(end) != null) {
            links.put(end, links.get(end) + 1);
        } else {
            links.put(end, 1);
        }
    }

    public void show() {
        for (String key: links.keySet()) {
            System.out.println("\t" + key + " -- " + links.get(key));
        }
    }

    public void addInsert() {
        insert++;
    }

    public int getInsert() {
        return insert;
    }
}
