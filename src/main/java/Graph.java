import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Graph {

    public static Graph graph = new Graph();

    private Graph() {

    }

    public static Graph getInstance() {
        return graph;
    }

    private Map<String, Site> links = new LinkedHashMap<String, Site>();

    void add(String begin, String end) {
        if (links.get(begin) == null) {
            links.put(begin, new Site(begin));
        }
        links.get(begin).add(end);
        Graph.getInstance().links.get(end).addInsert();
    }

    public void show() {
        for (String key: links.keySet()) {
            System.out.println(key + " входящих=" + links.get(key).getInsert());
            links.get(key).show();
        }
    }

    public void addAll(List<String> urls) {
        for (String url: urls) {
            links.put(url, new Site(url));
        }
    }
}
