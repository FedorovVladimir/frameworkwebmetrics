package action;

public interface Action {
    void run(Object document);
}
