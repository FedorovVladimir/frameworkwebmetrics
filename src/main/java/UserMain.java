import action.Action;
import framework.Site;
import framework.TypeSite;
import not_parse_action.NotParseAction;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class UserMain {
    public static void main(String[] args) {
        String url = "https://mdshow.ru";
        TypeSite typeSite = TypeSite.STATIC;

        Site site = new Site(url, typeSite);

//        site.setLocalLinksStrategy(new LocalLinksStrategy() {
//            public boolean analysis(String link, String baseLink) {
//                return false;
//            }
//        });

        site.setNotParseAction(new NotParseAction() {
            public void run(IOException e) {
                System.out.println("Ошибка");
            }
        });

        site.setAction(new Action() {
            public void run(Object document) {
                // TODO: 18.03.2019 приведение говно
                Elements elements = ((Document) document).getElementsByTag("title");
                for (Element e : elements) {
                    System.out.println(e.toString());
                }
            }
        });

//        site.setAction(new Counter());

        site.analysis();
    }
}
