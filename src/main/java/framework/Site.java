package framework;

import action.Action;
import action.EmptyAction;
import analizator.IteratorLink;
import analizator.LinksAnalyzer;
import local_links_strategy.LiteLocalLinksStrategy;
import local_links_strategy.LocalLinksStrategy;
import not_parse_action.EmptyNotParseAction;
import not_parse_action.NotParseAction;
import parser.Parser;
import parser.ParserFactory;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class Site {

    private String url;
    private TypeSite typeSite;
    private Action action = new EmptyAction();
    private LocalLinksStrategy localLinksStrategy = new LiteLocalLinksStrategy();
    private NotParseAction notParseAction = new EmptyNotParseAction();

    public Site(String url, TypeSite typeSite) {
        this.url = url;
        this.typeSite = typeSite;
    }

    public Site(TypeSite typeSite) {
        this.typeSite = typeSite;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public void setNotParseAction(NotParseAction notParseAction) {
        this.notParseAction = notParseAction;
    }

    public void setLocalLinksStrategy(LocalLinksStrategy localLinksStrategy) {
        this.localLinksStrategy = localLinksStrategy;
    }

    public void analysis() {
        Parser parser = ParserFactory.createParser(typeSite, notParseAction);
        LinksAnalyzer analyzer = new LinksAnalyzer(localLinksStrategy);

        IteratorLink iterator = new IteratorLink(Collections.singletonList("/"), parser, url, analyzer);
        while (iterator.hasNext()) {
            Object document = iterator.next();
            if (document != null) {
                action.run(document);
            }
        }
    }

    public void analysis(Set<String> pagesOnSite) {
        Parser parser = ParserFactory.createParser(typeSite, notParseAction);
        LinksAnalyzer analyzer = new LinksAnalyzer(new LocalLinksStrategy() {
            public boolean analysis(String link, String baseLink) {
                return false;
            }
        });

        List<String> urls = new LinkedList<String>();
        urls.addAll(pagesOnSite);
        IteratorLink iterator = new IteratorLink(urls, parser, "", analyzer);
        while (iterator.hasNext()) {
            Object document = iterator.next();
            if (document != null) {
                action.run(document);
            }
        }
    }
}
