package analizator;

import parser.Parser;

import java.util.LinkedList;
import java.util.List;

public class IteratorLink {

    private List<String> links = new LinkedList<String>();
    private Parser parser;
    private LinksAnalyzer analyzer;
    private String url;

    private int current = 0;

    public IteratorLink(List<String> links, Parser parser, String url, LinksAnalyzer analyzer) {
        this.links.addAll(links);
        this.parser = parser;
        this.url = url;
        this.analyzer = analyzer;
    }

    public boolean hasNext() {
        return current < links.size();
    }

    public Object next() {
        String link = links.get(current);
        List<String> localLinks = analyzer.getLocalLinks(parser.getLinks(url + link), url);
        for (String s: localLinks) {
            if (links.indexOf(s) == -1) {
                links.add(s);
            }
        }
        current++;
        return parser.getDoc();
    }

    public void remove() {
        links.remove(current);
    }

    public List<String> getLinks() {
        return links;
    }
}
