package analizator;

import local_links_strategy.LocalLinksStrategy;

import java.util.LinkedList;
import java.util.List;

public class  LinksAnalyzer {

    private LocalLinksStrategy localLinksStrategy;

    public LinksAnalyzer(LocalLinksStrategy localLinksStrategy) {
        this.localLinksStrategy = localLinksStrategy;
    }

    List<String> getLocalLinks(List<String> links, String baseLink) {
        List<String> list = new LinkedList<String>();
        for (String link: links) {
            if (localLinksStrategy.analysis(link, baseLink) && list.indexOf(link) == -1) {
                list.add(link);
            }
        }
        return list;
    }
}
