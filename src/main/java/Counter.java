import action.Action;

public class Counter implements Action {

    private int count;

    public void run(Object document) {
        System.out.println(count++);
    }
}
