package local_links_strategy;

public class LiteLocalLinksStrategy implements LocalLinksStrategy {
    public boolean analysis(String link, String baseLink) {
        return link.length() > 1 && link.charAt(0) == '/';
    }
}
