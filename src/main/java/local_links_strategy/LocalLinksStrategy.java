package local_links_strategy;

public interface LocalLinksStrategy {
    boolean analysis(String link, String baseLink);
}
