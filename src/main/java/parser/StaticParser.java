package parser;

import not_parse_action.NotParseAction;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

public class StaticParser implements Parser {

    private Document doc = null;
    private NotParseAction notParseAction;

    StaticParser(NotParseAction notParseAction) {
        this.notParseAction = notParseAction;
    }

    public List<String> getLinks(String url) {
        try {
            doc = Jsoup.connect(url).get();
            Elements elements = doc.getElementsByTag("a");
            List<String> urls = new LinkedList<String>();
            for (Element e : elements) {
                urls.add(e.attr("href"));
            }
            return urls;
        } catch (IOException e) {
            notParseAction.run(e);
        }
        return new LinkedList<String>();
    }

    public Object getDoc() {
        return doc;
    }
}
