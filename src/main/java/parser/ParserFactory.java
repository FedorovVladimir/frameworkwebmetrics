package parser;

import not_parse_action.NotParseAction;
import framework.TypeSite;

public class ParserFactory {
    public static Parser createParser(TypeSite typeSite, NotParseAction notParseAction) {
        if (typeSite == TypeSite.STATIC) {
            return new StaticParser(notParseAction);
        } else {
            return new DynamicParser(notParseAction);
        }
    }
}
