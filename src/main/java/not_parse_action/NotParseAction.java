package not_parse_action;

import java.io.IOException;

public interface NotParseAction {
    void run(IOException e);
}
