package not_parse_action;

import java.io.IOException;

public class EmptyNotParseAction implements NotParseAction {
    public void run(IOException e) {
        e.fillInStackTrace();
    }
}
