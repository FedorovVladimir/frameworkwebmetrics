import action.Action;
import framework.Site;
import framework.TypeSite;
import not_parse_action.NotParseAction;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UserMain2 {

    private static List<String> urls = new ArrayList<String>();
    private static Graph graph = Graph.getInstance();

    public static void main(String[] args) {
        urls.add("https://v9gel.github.io");
        urls.add("https://github.com/v9gel");
        urls.add("https://vk.com/wall48208058_7792");
        urls.add("http://project1230022.tilda.ws");

        graph.addAll(urls);

        analysisAll(urls.get(0));

        Set<String> set = new HashSet<String>();
        set.add("https://vk.com/wall48208058_7792");
        set.add("http://project1230022.tilda.ws");
        set.add("https://github.com/v9gel");
        analysis(set);

        System.out.println("This is Graph!");
        graph.show();
    }

    private static void analysis(Set<String> set) {
        Site site = new Site(TypeSite.STATIC);
        site.setNotParseAction(new NotParseAction() {
            public void run(IOException e) {
                System.out.println("Ошибка");
            }
        });
        site.setAction(new Action() {
            public void run(Object document) {
                Elements elements = ((Document) document).getElementsByTag("a");
                for (Element e : elements) {
                    add(e.attr("href"), ((Document) document).baseUri());
                }
                for (Element e : elements) {
                    add(e.text(), ((Document) document).baseUri());
                }
            }
        });
        site.analysis(set);
    }

    private static void analysisAll(final String url) {
        Site site = new Site(url, TypeSite.STATIC);
        site.setNotParseAction(new NotParseAction() {
            public void run(IOException e) {
                System.out.println("Ошибка");
            }
        });
        site.setAction(new Action() {
            public void run(Object document) {
                Elements elements = ((Document) document).getElementsByTag("a");
                for (Element e : elements) {
                    add(e.attr("href"), url);
                }
                for (Element e : elements) {
                    add(e.text(), ((Document) document).baseUri());
                }
            }
        });
        site.analysis();
    }

    private static void add(String href, String baseUrl) {
        if (isExternal(href, baseUrl)) {
            href = parseHref(href);
//            System.out.println(baseUrl + " --> " + href);
            graph.add(baseUrl, href);
        }
    }

    private static String parseHref(String href) {
        for (String url: urls) {
            if (href.regionMatches(0, url, 0, url.length())) {
                return url;
            }
        }
        return null;
    }

    private static boolean isExternal(String href, String baseUrl) {
        boolean result = false;
        for (String url: urls) {
            result = result || href.regionMatches(0, url, 0, url.length());
        }
        return result && !href.regionMatches(0, baseUrl, 0, baseUrl.length());
    }
}
